// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBw5f9XSpLN_Fffa0LCParLfaE7cnLTlOo",
    authDomain: "phonekey-c8e3e.firebaseapp.com",
    databaseURL: "https://phonekey-c8e3e.firebaseio.com",
    projectId: "phonekey-c8e3e",
    storageBucket: "phonekey-c8e3e.appspot.com",
    messagingSenderId: "944820168632",
    appId: "1:944820168632:web:bed0ce855ee378a3c7114d",
    measurementId: "G-W4BQ4FEMXV"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
