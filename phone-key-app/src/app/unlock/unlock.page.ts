import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ToastController } from '@ionic/angular';
import { Session } from '../session.model';
import { AuthService } from '../auth/auth.service';
import { Observable } from 'rxjs';
import { Plugins } from '@capacitor/core';

@Component({
  selector: 'app-unlock',
  templateUrl: './unlock.page.html',
  styleUrls: ['./unlock.page.scss'],
})
export class UnlockPage implements OnInit {
  _session: Session;
  
  sessionDoc: AngularFirestoreDocument<Session>;
  isScannerReady = false;
  isAuthenticated = false;

  constructor(
    private toastCtrl: ToastController,
    private db: AngularFirestore,
    private auth: AuthService
  ) { }

  ngOnInit() {
  }

  onSuccess(event: any) {
    window.navigator.vibrate(500);
    console.log('Success Event: ', event);
    this.db.collection<Session>('sessions', ref => ref.where('id', '==', event)).valueChanges().subscribe(res => {
      this._session = res[0];
      console.log('Session: ', this._session);
    });
    // this._session = this.sessionDoc.valueChanges();
    // this._session.subscribe(res => {
    //   console.log("sesh: ", res);
    // });
  }

  onFound(event: any) {
    this.toastCtrl.create({
      message: 'Camera found',
      duration: 3000
    }).then(toastEl => toastEl.present()).catch(err => console.log(err));
    this.isScannerReady = true;
  }

  onNotFound(event: any) {
    console.log('Not found Event: ', event);
  }

  onContinue() {
    const updatedSession: Session = {
      id: this._session.id,
      domain: this._session.domain,
      User: {
        email: "",
        displayName: "",
        uid: "",
        photoURL: "",
        returnedAt: ""
      },
      createdAt: this._session.createdAt,
      location: this._session.location,
      keyData: {
        location: {
          lat: 0,
          lng: 0
        },
        device: {
          appVersion: "",
          batteryLevel: 0,
          isCharging: false,
          isVirtual: false,
          manufacturer: "",
          model: "",
          osVersion: "",
          platform: "",
          uuid: ""
        },
        AllowedDomains: ["www.collabhive.co.za"]
      },
      address: this._session.address,
      isValid: this._session.isValid
    };

    if (!Plugins.Geolocation) {
      console.log('Geolocation Unavailable');
      return;
    } else {
      Plugins.Geolocation.getCurrentPosition().then(coords => {
        updatedSession.keyData.location.lat = coords.coords.latitude;
        updatedSession.keyData.location.lng = coords.coords.longitude;
        Plugins.Device.getInfo().then(res => {
          updatedSession.keyData.device.appVersion = res.appVersion;
          updatedSession.keyData.device.batteryLevel = res.batteryLevel;
          updatedSession.keyData.device.isCharging = res.isCharging;
          updatedSession.keyData.device.isVirtual = res.isVirtual;
          updatedSession.keyData.device.manufacturer = res.manufacturer;
          updatedSession.keyData.device.model = res.model;
          updatedSession.keyData.device.osVersion = res.osVersion;
          updatedSession.keyData.device.platform = res.platform;
          updatedSession.keyData.device.uuid = res.uuid;
          this.auth.user$.subscribe(response => {
            updatedSession.User.device = response.device;
            updatedSession.User.displayName = response.displayName;
            updatedSession.User.email = response.email;
            updatedSession.User.photoURL = response.photoURL;
            const sessionRef = this.db.doc(`sessions/${this._session.id}`);
            console.log('Updated session: ', updatedSession);
            sessionRef.set(updatedSession).then(() => {
              console.log('Session updated, authentication successfull.');
              this.isAuthenticated = true;
            }).catch(err => console.log(err));
          });
        }).catch(err => console.log(err));
      }).catch(err => console.log(err));
    }
  }

}
