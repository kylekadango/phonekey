import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { UnlockPageRoutingModule } from './unlock-routing.module';
import { UnlockPage } from './unlock.page';

import { ZXingScannerModule } from '@zxing/ngx-scanner';

@NgModule({
  imports: [
    ZXingScannerModule,
    CommonModule,
    FormsModule,
    IonicModule,
    UnlockPageRoutingModule
  ],
  declarations: [UnlockPage]
})
export class UnlockPageModule {}
