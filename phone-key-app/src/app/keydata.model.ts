import { Location } from "./location.model";
import { Device } from './device.model';

export interface KeyData {
  location: Location;
  device: Device;
  AllowedDomains: string[];
}
