export interface Device {
    appVersion: string;
    batteryLevel: number;
    isCharging: boolean;
    isVirtual: boolean;
    manufacturer: string;
    model: string;
    osVersion: string;
    platform: string;
    uuid: string;
}