# pk-login-component



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type      | Default     |
| ---------- | ----------- | ----------- | --------- | ----------- |
| `domain`   | `domain`    |             | `string`  | `undefined` |
| `testMode` | `test-mode` |             | `boolean` | `undefined` |


## Events

| Event     | Description | Type                  |
| --------- | ----------- | --------------------- |
| `login`   |             | `CustomEvent<User>`   |
| `signOut` |             | `CustomEvent<Object>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
