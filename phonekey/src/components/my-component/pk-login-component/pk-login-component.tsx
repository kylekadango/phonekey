import { Component, Prop, h, Event, EventEmitter, State } from '@stencil/core';
import { Session } from './session.model';
import { User } from './user.model';
import { Plugins } from '@capacitor/core';
import * as firebase from 'firebase';

var app = firebase.initializeApp({
  apiKey: "AIzaSyBw5f9XSpLN_Fffa0LCParLfaE7cnLTlOo",
  authDomain: "phonekey-c8e3e.firebaseapp.com",
  databaseURL: "https://phonekey-c8e3e.firebaseio.com",
  projectId: "phonekey-c8e3e",
  storageBucket: "phonekey-c8e3e.appspot.com",
  messagingSenderId: "944820168632",
  appId: "1:944820168632:web:bed0ce855ee378a3c7114d",
  measurementId: "G-W4BQ4FEMXV"
});
import 'firebase/firestore';

@Component({
  tag: 'pk-login-component',
  styleUrl: 'pk-login-component.css',
  shadow: true,
})
export class PkLoginComponent {
  @Prop() testMode: boolean;
  @Prop() domain: string;
  _session: any;
  session: Session = {
    id: null,
    domain: null,
    User: null,
    createdAt: new Date,
    location: {lng: null, lat: null},
    keyData: null,
    address: null,
    isValid: null
  };
  @State() isBarcodeReady = false;
  src: string;
  @State() isAuthenticated = false;

  constructor() {
    console.log('TestMode: ', this.testMode);
    console.log('Creating session with domain: ', this.domain);
    const sessionsRef = app.firestore().collection('sessions');
    this.session.domain = this.domain;
    sessionsRef.add(this.session).then(docRef => {
      console.log('Session Data: ', this.session);
      console.log('Session added, docref: ', docRef);
      this.session.id = docRef.id;
      this.session.isValid = true;

      if (!Plugins.Geolocation) {
        console.log('Geolocation Unavailable');
        return;
      } else {
        Plugins.Geolocation.getCurrentPosition().then(res => {
          console.log('Res: ', res);
          this.session.location.lat = res.coords.latitude;
          this.session.location.lng = res.coords.longitude;
        }).catch(err => console.log(err));
      }

      const sessionRef = app.firestore().doc(`sessions/${this.session.id}`);
      sessionRef.set(this.session).then(() => {
        console.log('Session updated');
        console.log('SessionData: ', this.session);
        this.src = "https://api.qrserver.com/v1/create-qr-code/?data=" + this.session.id + "&amp;size=150x150";
        this.isBarcodeReady = true;
        app.firestore().collection('sessions').doc(this.session.id).onSnapshot(() => {
          sessionRef.get().then(doc => {
            this._session = doc.data();
            console.log('_Session: ', this._session);
            console.log('doc: ', doc);
            console.log('docData: ', doc.data());
            if (this._session.User) {
              console.log('We are within');
              this.loginHandler();
              this.isAuthenticated = true;
              this.isBarcodeReady = false;
            }
          }).catch(err => console.log(err));
        });
      }).catch(err => console.log(err));
    }).catch(err => console.log(err));
  }

  @Event() login: EventEmitter<User>;

  loginHandler() {
    this.login.emit(this._session.User);
  }

  @Event() signOut: EventEmitter<Object>;

  signOutHandler() {
    this.signOut.emit({action: 'User signed out'});
  }

  render() {
    if (!this.isBarcodeReady && !this.isAuthenticated) {
      return (<div>We are preparing the session</div>)
    } else if (this.isBarcodeReady) {
      return (<div><img id='barcode' src={this.src} alt="" title="HELLO" width="150" height="150" /> <br/><span>{this.session.id}</span> <br /> <span>Lat: {this.session.location.lat}</span> <br /> <span>Lng: {this.session.location.lng}</span> <br /> Please scan to login..</div>)
    } else if (!this.isBarcodeReady && this.isAuthenticated) {
    return (<div><h1>You are logged in as:</h1> <br/> Name: {this._session.User.displayName} <br/> Email: {this._session.User.email} <br/> Device: {this._session.keyData.device.model} <br/> Location: <br/> Lat: {this._session.keyData.location.lat}, Lng: {this._session.keyData.location.lng}</div>)
    }

    // if (!this.isBarcodeReady) {
    //   return ( <div>Hello {this.name}</div> )
    // } else {
    //   return ( <div>Hello, World</div> )
    // }
    // return <div>Hello, World! I'm {this.getText()}</div>;
    // return <div>Hello, World! I'm</div>;
  }

}
