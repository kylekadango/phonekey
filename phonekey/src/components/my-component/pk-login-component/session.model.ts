import { Location } from "./location.model";
import { KeyData } from "./keydata.model";
import { User } from "./user.model";

export interface Session {
  id: string;
  domain: string;
  User: User;
  createdAt: Date;
  location: Location;
  keyData: KeyData;
  address: string;
  isValid: boolean;
}
